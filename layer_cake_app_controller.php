<?php class LayerCakeAppController extends AppController {

	var $helpers    = array( 'Html', 'Form', 'Session', 'LayerCake.Cycle', 'LayerCake.ThisUrl' );
    var $components = array( 'Session', 'Auth', 'LayerCake.PersistentValidation' );

	function beforeFilter() {
        parent::beforeFilter( );

        $this->Auth->userModel   	= 'Admin';
		$this->Auth->loginRedirect  = '/admin/dashboard';
		$this->Auth->logoutRedirect = '/admin/admins/login';

		if( isset( $this->params['prefix'] ) && $this->params['prefix'] == 'admin' ) {
            $this->layout = 'admin_default';
        }
    }
} ?>