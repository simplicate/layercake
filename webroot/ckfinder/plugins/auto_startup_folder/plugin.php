﻿<?php
	// if we're authenticated, lets make sure that the folder we're asking for exists
	if( CheckAuthentication() ) {

		// parse the query string
		parse_str( $_SERVER['QUERY_STRING'], $qs );

		// compile the full path to the folder we want to start-up
		$startUpDir = $baseDir . '/' . strtolower( $qs['type'] ) . '/' . $qs['currentFolder'];
		$startUpDir = rtrim( $startUpDir, "/" );
		$startUpDir = str_replace("//", "/", $startUpDir );

		// if the directory doesn't exist, automatically create it
		if( !is_dir( $startUpDir ) ) {
			mkdir( $startUpDir, $config['ChmodFolders'] );
		}

		// $file = fopen("/home/domains/beta.gentlemensexpo.com/test.log","w");
		// echo fwrite( $file, print_r( $startUpDir, true ) );
		// echo fwrite( $file, print_r( $_SERVER, true ) );
		// fclose($file);
	}
?>