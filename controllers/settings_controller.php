<? App::import( 'Controller', 'LayerCake.LayerCakeApp' ); ?>
<? class SettingsController extends LayerCakeAppController {

	var $name       = 'Settings';
    var $helpers    = array( 'Html', 'Form', 'Session', 'LayerCake.Cycle', 'Xml' );
    var $components = array( 'Session', 'Auth', 'Email', 'Content.PageContent' );
    var $uses       = array( 'LayerCake.Setting' );

    // admin index
    function admin_index() {

        $this->paginate = array( );

		$this->Setting->recursive = 1;
		$this->set( 'settings', $this->paginate( 'Setting' ) );
	}


    function admin_cache_clear() {

        Cache::clear();
        clearCache();

        $this->Session->setFlash( 'Cache Cleared', 'default', array( 'class' => 'success' ) );
        $this->redirect( '/admin/settings' );

    }


	// admin - edit an existing record
    function admin_edit( $id = null ) {
		if( !$id && empty($this->data) ) {
			$this->Session->setFlash(__('Invalid Record', true), 'default', array( 'class' => 'error' ) );
			$this->redirect( $this->referer() );
		}

		// where to go after saving
        if( strstr( $this->referer(), '/settings/index' ) || $this->referer() == '/admin/settings/' || $this->referer() == '/admin/settings' ) {
			$this->Session->write( "History.Setting.Edit." . $id, $this->referer() );
		}

        // save record
        $this->admin_save();

        // output form
        $this->admin_form( $id );
	}


    // admin - output form
    private function admin_form( $id = null ) {

        // load record to edit
        if( isset( $id ) & empty($this->data) ) { $this->data = $this->Setting->read( null, $id ); }

        // output form
        $this->render( 'admin_form' );
    }


    // admin - save record
    private function admin_save() {

        // don't bother saving unless there's data to save
        if( !empty($this->data) ) {

            $this->Setting->create();

            // try to save the record
            if( $this->Setting->save( $this->data ) ) {

                // redirect to success page
                $this->Session->setFlash(__('The Record has been saved', true), 'default', array( 'class' => 'success' ) );
                $history  = ( $this->data['Setting']['id'] ) ? $this->Session->read( "History.Setting.Edit." . $this->data['Setting']['id'] ) : $this->Session->read( "History.Setting.Add" );
                $this->redirect( isset( $history ) ? $history : array( 'action' => 'index' ) );

            // bad save
            } else {
                $this->Session->setFlash(__('The Record could not be saved. Please, try again.', true), 'default', array( 'class' => 'error' ) );
            }
		}
    }
}