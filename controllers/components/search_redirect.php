<?php
    class SearchRedirectComponent extends Object {

        // called before Controller::beforeFilter()
        function initialize( &$controller, $settings = array() ) {

            // saving the controller reference for later use
            $this->controller =& $controller;

        }


        // this function will redirect a form post or get request to use named parameters
        // so that the url can be copy / pasted, generally for use in search result pages
        function redirectParams( $model = '', $listOfParams = array(), $prefixUrl = '' ) {

            // do we need to redirect
            $needRedirect = false;
            $redirectUrl  = str_replace( "//", "/", $prefixUrl . "/" );
            $searchParams = array();

            // get list of params
            foreach( $listOfParams AS $param ) {

                // from URL ?query=string
                if( isset( $this->controller->params['url'][$param] ) && !empty( $this->controller->params['url'][$param] ) ) {
                    $searchParams[$param] = $this->controller->params['url'][$param];
                    $needRedirect = true;

                // as post variable ['Model']['var']
                } elseif( !empty( $model ) && isset( $this->controller->data[$model][$param] ) && !empty( $this->controller->data[$model][$param] ) ) {
                    $searchParams[$param] = $this->controller->data[$model][$param];
                    $needRedirect = true;

                // as post variable without ['Model']
                } elseif( isset( $this->controller->data[$param] ) && !empty( $this->controller->data[$param] ) ) {
                    $searchParams[$param] = $this->controller->data[$param];
                    $needRedirect = true;

                // as post variable in ['form']
                } elseif( isset( $this->controller->params['form'][$param] ) && !empty( $this->controller->params['form'][$param] ) ) {
                    $searchParams[$param] = $this->controller->params['form'][$param];
                    $needRedirect = true;

                // as a /named:param/
                } elseif( isset( $this->controller->params['named'][$param] ) && !empty( $this->controller->params['named'][$param] ) ) {
                    $searchParams[$param] = $this->controller->params['named'][$param];
                }
            }

            // redirect to a new url with named params
            if( $needRedirect == true ) {

                foreach( array_keys( $searchParams ) AS $param ) {
                    $redirectUrl .= $param . ':' . $searchParams[$param] . "/";
                }

                $this->controller->redirect( $redirectUrl );

            // return the found search params
            } else {
                return $searchParams;
            }
        }

    }
?>