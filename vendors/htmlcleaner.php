<?
    require( 'htmlcleaner/phpQuery.php' );
    require( 'htmlcleaner/htmLawed.php' );

    class htmlcleaner {

        /*
        *   looks for <img> tags with an align="left|right" or style="float: left|right;"
        *   and tacks on a class="align-left|right"
        */
        function clean_img_floats( $html ) {
            $doc = phpQuery::newDocument( $html );
            phpQuery::selectDocument($doc);

            foreach( pq('img') as $img ) {
                $style = strtolower( pq($img)->attr('style') );
                $align = strtolower( pq($img)->attr('align') );

                if( strstr( $style, 'float: right;' ) || $align == 'right' ) {
                    pq($img)->removeClass( 'align-left' );
                    pq($img)->addClass( 'align-right' );
                }

                if( strstr( $style, 'float: left;'  ) || $align == 'left'  ) {
                    pq($img)->removeClass( 'align-right' );
                    pq($img)->addClass( 'align-left' );
                }
            }

            $html = mb_convert_encoding( $doc, 'HTML-ENTITIES', "UTF-8" );

            return $html;
        }


        /*
        *   string inline styles and font tags
        */
        function clean_inline_styles( $html ) {

            $html = preg_replace( "/<font[^>]+\>/i", "", $html );
            $html = preg_replace( "/<\/font>/i",     "", $html );

            $html = preg_replace( '/<span><span>/i',                '',         $html );
            $html = preg_replace( '/<style[^>]*>.*<\/style>/',      '',         $html );
            $html = preg_replace( '/<p>[\n\r\s\t]+/i',              '<p>',      $html );
            $html = preg_replace( '/<p[^>]*>(\s|&nbsp;?)*<\/p>/',   '',         $html );

            $html = $this->clean_html( $html );

            return $html;
        }


        /*
        *   run the input through html tidy
        */
        function clean_html( $input_html = "" ) {

            //$cleaner = new htmLawed();

            $input_html  = preg_replace( "/<(element[^>]+)\>/i", "[$1]", $input_html );
            $input_html  = preg_replace( "/<(snippet[^>]+)\>/i", "[$1]", $input_html );

            $output      = htmLawed(
                            $input_html
                            , array(
                                "tidy"              => 1,
                                "clean_ms_char"     => 2,
                                "cdata"             => 2,
                                //"deny_attribute"    => "* -id -href -align -style -data-href -width -rel -title -height -alt -name -rowspan -colspan -valign -src -target -class -value -type -method -action -checked -enctype -cellpadding -cellspacing -script -xml:space",
                                "elements"          => "* +script +iframe +meta +article +section +nav",
                                "unique_ids"        => 1,
                            )
                       );

            $output  = preg_replace( "/\[(element[^\]]+)\]/i", "<$1>", $output );
            $output  = preg_replace( "/\[(snippet[^\]]+)\]/i", "<$1>", $output );

            return $output;
        }
    }
?>