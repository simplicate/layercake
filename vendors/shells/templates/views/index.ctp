<h2 class="gt-table-head"><?php echo "<?php __('{$pluralHumanName}');?>";?></h2>
<div class="gt-content-box">
    <table border="0" class="gt-table">
        <thead>
            <tr>
                <?php  foreach ($fields as $field):?>
                <th><?php echo "<?php echo \$paginator->sort('{$field}');?>";?></th>
                <?php endforeach;?>
                <th class="actions"><?php echo "<?php __('Actions');?>";?></th>          
            </tr>
        </thead>
        <tbody>
            <?php
            echo "<?php
            \$i = 0;
            foreach (\${$pluralVar} as \${$singularVar}):
                \$class = null;
                if (\$i++ % 2 == 0) {
                    \$class = ' class=\"altrow\"';
                }
            ?>\n";
                echo "\t<tr<?php echo \$class;?>>\n";
                    foreach ($fields as $field) {
                        $isKey = false;
                        if (!empty($associations['belongsTo'])) {
                            foreach ($associations['belongsTo'] as $alias => $details) {
                                if ($field === $details['foreignKey']) {
                                    $isKey = true;
                                    echo "\t\t<td>\n\t\t\t<?php echo \$html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t</td>\n";
                                    break;
                                }
                            }
                        }
                        if ($isKey !== true) {
                            echo "\t\t<td>\n\t\t\t<?php echo \${$singularVar}['{$modelClass}']['{$field}']; ?>\n\t\t</td>\n";
                        }
                    }
            
                    echo "\t\t<td class=\"actions\">\n";
                    echo "\t\t\t<?php echo \$html->link(__('Edit', true), array('action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}'])); ?>\n";
                    echo "\t\t\t<?php echo \$html->link(__('Delete', true), array('action' => 'delete', \${$singularVar}['{$modelClass}']['{$primaryKey}']), null, sprintf(__('Are you sure you want to delete # %s?', true), \${$singularVar}['{$modelClass}']['{$primaryKey}'])); ?>\n";
                    echo "\t\t</td>\n";
                echo "\t</tr>\n";
            
            echo "<?php endforeach; ?>\n";
            ?>
        </tbody>
    </table>
</div>

<div class="gt-table-controls gt-table-controls-btm clearfix">
    <p class="gt-table-pager"><?php echo "\t<?php echo \$paginator->numbers();?>\n"?></p>
</div>

<div class="gt-table-buttons">
    <?php echo "<?php echo \$html->link(__('New {$singularHumanName}', true), array('action' => 'add' ), array( 'class' => 'gt-btn-green-large gt-btn-left' )); ?>\n";?>
    
</div>