<h2 class="gt-form-head"><?php echo "<?php __('" . Inflector::humanize($action) . " {$singularHumanName}');?>";?></h2>
<div class="gt-form gt-content-box">
    <div class="clearfix">

    <?php echo "<?php echo \$form->create('{$modelClass}');?>\n";?>
    
    <div class="gt-left-col">      
        <?php
                echo "\t<?php\n";
                foreach ($fields as $field) {
                    if ($action == 'add' && $field == $primaryKey) {
                        continue;
                    } elseif (!in_array($field, array('created', 'modified', 'updated'))) {
                        echo "\t\techo \"<div class='gt-form-row gt-width-66'>\";\n";
                        echo "\t\techo \$form->input('{$field}', array( 'class' => 'gt-form-text' ));\n";
                        echo "\t\techo \"</div>\";\n";
                    }
                }
                if (!empty($associations['hasAndBelongsToMany'])) {
                    foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
                        echo "\t\techo \"<div class='gt-form-row gt-width-66'>\";\n";
                        echo "\t\techo \$form->input('{$assocName}');\n";
                        echo "\t\techo \"</div>\";\n";
                    }
                }
                echo "\t?>\n";
        ?>
    </div>
    
    <div class="gt-right-col">
        <br />
        <br />
        <?php
            echo "\t<?php\n";
            
            echo "\t?>\n";
        ?>
    </div>
</div>
<br />
<div class="gt-form-row">
    <?php
        echo "<?php echo \$form->end( array( 'label' => 'Submit', 'class' => 'gt-btn-green-small gt-btn-left' ) );?>\n";
    ?>
    
    <p class="gt-cancel">or <?php echo "<?php echo \$html->link( 'cancel', array( 'action' => 'index') );?>\n"; ?></p>
</div>