<?
    $this->addScript( $this->Html->script( "/layer_cake/ckeditor/ckeditor.js" ) );
    $this->addScript( $this->Html->script( "/assets/ckfinder/ckfinder.js" ) );

    $types = array();
    if( is_array( $type ) ) { $types   = $type; }
    else                    { $types[] = $type; }
?>

<script type="text/javascript">
    $(function() {

        CKEDITOR.stylesSet.add('my_styles', [
            { name : 'Float Left',   element : 'div',  attributes : { 'class' : 'float-left'   } },
            { name : 'Float Right',  element : 'div',  attributes : { 'class' : 'float-right'  } },
            { name : 'Center',       element : 'div',  attributes : { 'class' : 'float-center' } }
        ]);

        CKEDITOR.on( 'instanceReady', function( ev ) {

            ev.editor.dataProcessor.writer.setRules( "p", {
                indent			: false,
                breakBeforeOpen	: true,
                breakAfterOpen	: false,
                breakBeforeClose: false,
                breakAfterClose	: true
            });

            ev.editor.dataProcessor.writer.setRules( "ul", {
                indent			: false,
                breakBeforeOpen	: true,
                breakAfterOpen	: false,
                breakBeforeClose: false,
                breakAfterClose	: true
            });

            ev.editor.dataProcessor.writer.setRules( "ol", {
                indent			: false,
                breakBeforeOpen	: true,
                breakAfterOpen	: false,
                breakBeforeClose: false,
                breakAfterClose	: true
            });

            ev.editor.dataProcessor.writer.setRules( "h1", {
                indent			: false,
                breakBeforeOpen	: true,
                breakAfterOpen	: false,
                breakBeforeClose: false,
                breakAfterClose	: true
            });

            ev.editor.dataProcessor.writer.setRules( "h2", {
                indent			: false,
                breakBeforeOpen	: true,
                breakAfterOpen	: false,
                breakBeforeClose: false,
                breakAfterClose	: true
            });

            ev.editor.dataProcessor.writer.setRules( "h3", {
                indent			: false,
                breakBeforeOpen	: true,
                breakAfterOpen	: false,
                breakBeforeClose: false,
                breakAfterClose	: true
            });

            ev.editor.dataProcessor.writer.setRules( "h4", {
                indent			: false,
                breakBeforeOpen	: true,
                breakAfterOpen	: false,
                breakBeforeClose: false,
                breakAfterClose	: true
            });

            ev.editor.dataProcessor.writer.setRules( "h5", {
                indent			: false,
                breakBeforeOpen	: true,
                breakAfterOpen	: false,
                breakBeforeClose: false,
                breakAfterClose	: true
            });

            ev.editor.dataProcessor.writer.setRules( "h6", {
                indent			: false,
                breakBeforeOpen	: true,
                breakAfterOpen	: false,
                breakBeforeClose: false,
                breakAfterClose	: true
            });

            ev.editor.dataProcessor.writer.setRules( "li", {
                indent			: true,
                breakBeforeOpen	: true,
                breakAfterOpen	: false,
                breakBeforeClose: false,
                breakAfterClose	: true
            });
        });

        <? if( in_array( 'regular', $types ) ): ?>
            $.each( $('textarea.ck-full'), function(index, item) {
                CKFinder.setupCKEditor(
                    CKEDITOR.replace( $(item).attr('id'), {
                        toolbar :
                        [
                            ['Cut','Copy','Paste','PasteText','PasteFromWord','RemoveFormat'],['Undo','Redo'],
                            ['Find','Replace'],
                            ['Image','Flash','Table','HorizontalRule','SpecialChar'],
                            ['Link','Unlink','Anchor'],
                            ['Source'],
                            '/',
                            ['Format', 'Styles'],
                            ['Bold','Italic','TextColor','FontSize','Subscript','Superscript'],
                            ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],
                            ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock']
                        ],
                        resize_dir : 'vertical',
                        height: '450px',
                        stylesSet: 'my_styles',
                        baseHref : '/',
                        contentsCss : "/layer_cake/css/ckeditor.css"
                    }), '/assets/ckfinder/'
                );
            });
        <? endif; ?>

        <? if( in_array( 'simple', $types ) ): ?>
            $.each( $('textarea.ck-simple'), function(index, item) {
                CKFinder.setupCKEditor(
                    CKEDITOR.replace( $(item).attr('id'), {
                        toolbar : [
                            ['Bold','Italic','TextColor','FontSize'],
                            ['NumberedList','BulletedList'],
                            ['Image','HorizontalRule','RemoveFormat'],
                            ['Link','Unlink'],
                            ['Source']
                        ],
                        resize_dir : 'vertical',
                        baseHref : '/',
                        contentsCss : "/layer_cake/css/ckeditor.css"
                    }), '/assets/ckfinder/'
                );
            });
        <? endif; ?>
    });
</script>