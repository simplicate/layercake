<h1>Settings</h1>

<?
    $paginate_opts           = $this->passedArgs;
    $paginate_opts['plugin'] = NULL;
    $this->Paginator->options( array( 'url' => $paginate_opts ) );
?>

<?= $html->link( "Clear Cache", '/admin/settings/cache_clear', array( 'class' => 'button', 'escape' => false )  ); ?>

<? if( sizeof( $settings ) >= 1 ): ?>
    <table class="list">
        <thead>
            <tr>
                <th><?= $paginator->sort('name');?></th>
                <th><?= $paginator->sort('Coders Reference', 'slug');?></th>
                <th width="50%"><?= $paginator->sort('content');?></th>
                <th class="t-center">Actions</th>
            </tr>
        </thead>

        <tbody>
            <? foreach( $settings AS $setting ): ?>
                <tr class="<?= $cycle->cycle('', 'bg'); ?>">
                    <td><?= $setting['Setting']['name']; ?></td>
                    <td><?= $setting['Setting']['slug']; ?></td>
                    <td>
                        <? if( $setting['Setting']['type'] == 'text' ): ?>
                            <?= $setting['Setting']['content']; ?>
                        <? elseif( $setting['Setting']['type'] == 'textarea' ): ?>
                            <?= rtrim( substr( $setting['Setting']['content'], 0, 150 ), " \t." ); ?><? if( strlen( $setting['Setting']['content'] ) > 150 ): ?>...<? endif; ?>
                        <? elseif( $setting['Setting']['type'] == 'select' ): ?>
                            <?
                                $json = json_decode( $setting['Setting']['structure'], true );
                                echo $json['options'][$setting['Setting']['content']];
                            ?>
                        <? endif; ?>
                    </td>
                    <td class="t-center icons">
                        <?= $html->link('<img src="/layer_cake/images/ico-mini-edit.png" alt="Edit">', array( 'action' => 'edit', $setting['Setting']['id']), array( 'escape' => false ) ); ?>
                    </td>
                </tr>
            <? endforeach; ?>
        </tbody>
    </table>

    <? if( $this->params['paging']['Setting']['pageCount'] > 1 ): ?>
        <!-- Pagination -->
        <div class="pagination bottom box">
            <p class="f-left"><?= $paginator->counter(); ?></p>

            <p class="f-right">
                <?= $paginator->prev('«'); ?>
                <?= $paginator->numbers();		 ?>
                <?= $paginator->next('»'); ?>
            </p>
        </div>
    <? endif; ?>
<? else: ?>

    <h3>No records found that match your search criteria.</h3>
    <p><?= $html->link( 'Reset Search', array('action' => 'index' ) ); ?></p>

<? endif; ?>