<h1>Edit Setting</h1>

<?= $form->create( 'Setting', array( 'id' => 'edit-form', 'type' => 'file' ) );?>

<?= $form->input( 'id' ); ?>

<?
    // json structure
    $json     = json_decode( $this->data['Setting']['structure'], true );
    $required = ( isset( $json['required'] ) && $json['required'] == true ) ? 'required'      : '';
    $format   = ( !empty( $json['format'] ) ) ? $json['format'] : '';
    $class    = $required . ' ' . $format;
    $after    = ( !empty( $json['note'] ) ) ? "<div class='small'>" . $json['note'] . "</div>" : '';
    $empty    = ( !empty( $json['empty'] ) ) ? $json['empty'] : '';

    $options  = array();
    if( !empty( $json['options'] ) ) {
        $options = $json['options'];
    }
?>

<div class="form-left">
    <h2><?= $this->data['Setting']['name']; ?></h2>

    <? if( $this->data['Setting']['type'] == 'text' ): ?>
        <?= $form->input( 'content', array( 'label' => false, 'type' => 'text', 'class' => $class, 'required' => $required, 'after' => $after ) ); ?>
    <? elseif( $this->data['Setting']['type'] == 'textarea' ): ?>
        <?= $form->input( 'content', array( 'label' => false, 'type' => 'textarea', 'class' => $class, 'required' => $required, 'after' => $after ) ); ?>
    <? elseif( $this->data['Setting']['type'] == 'select' ): ?>
        <?= $form->input( 'content', array( 'label' => false, 'type' => 'select', 'class' => $class, 'required' => $required, 'after' => $after, 'empty' => $empty, 'options' => $options ) ); ?>
    <? endif; ?>
</div>

<div class="clear"></div>

<br><br>
<?= $form->end( array( 'label' => 'Save' ) );?>
<?= $this->element( 'cancel_link',  array( "HistoryModel" => "Setting" ) ); ?>

<script type="text/javascript">
    $( document ).ready(function() {
        $('#edit-form').validate({});
    });
</script>