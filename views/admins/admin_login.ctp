<? $this->set( 'title_for_layout', "Login" ); ?>

<?= $form->create( "Admin", array( 'action' => 'login' ) ); ?>
<?= $form->input( "username", array( "tabindex" => 1 ) ); ?>
<?= $form->input( "password", array( "tabindex" => 2 ) ); ?>
    
<? if( 0 ): ?>
    <div class="input">
        <label><?= $form->checkbox( "remember" ); ?> Remember me</label>
    </div>
<? endif; ?>
    
<?= $form->end( array( "tabindex" => 3, "label" => "Login" ) ); ?>
<p class="spacer"><a href="/admin/admins/forgot">Forgot your password?</a></p>