<h1>Edit Account Settings</h1>

<?= $form->create('Admin', array( 'autocomplete' => 'off' ) ); ?>
<?= $form->input('id' ); ?>

<div class="form-left">
    <?= $form->input('username'); ?>
    <?= $form->input('name'); ?>
    <?= $form->input('email'); ?>
    
    <?= $form->input( 'new_password_1', array( 'type' => 'password', 'label' => 'New Password' ) ); ?>
    <?= $form->input( 'new_password_2', array( 'type' => 'password', 'label' => 'Repeat Password' ) ); ?>
</div>
<div class="clear"></div>

<?= $form->end( array( 'label' => 'Save' ) );?>
<p class="spacer">or ...</p>
<p class="cancel"><a href="/admin/dashboard">cancel</a></p>


