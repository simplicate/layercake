<? $this->set( 'title_for_layout', "Forgot Your Password" ); ?>

<?= $form->create( "Admin", array( "action" => "forgot" ) ); ?>
<?= $form->input( "email" ); ?>

<?= $form->end( array( "label" => "Submit" ) ); ?>

<p class="spacer"><a href="/admin/admins/login">Return to Login</a></p>