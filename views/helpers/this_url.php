<?php
class ThisUrlHelper extends AppHelper {

    public function bare() {
        $url = rtrim( Router::url( $this->here, true ), "/" );

        if( isset( $this->params['named'] ) ) {
            foreach( $this->params['named'] AS $named => $value ) {
                $url = str_replace( "/$named:$value", "", $url );
            }
        }

        return $url;
	}
}